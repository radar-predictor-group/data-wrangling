import argparse
import os
import pathlib
import time

from image_processing import process_all


def recreate_directory_structure(source_path, dest_path):
    pathlib.Path(dest_path).mkdir(parents=True, exist_ok=True)
    for _, dirs, _ in os.walk(source_path):
        for dir in dirs:
            pathlib.Path(os.path.join(dest_path, dir)).mkdir(parents=True, exist_ok=True)


def get_parsed_args():
    parser = argparse.ArgumentParser(
        description='Processes all images found in the source path to the destination path')
    parser.add_argument('--source', '-s', default='./original', type=str, help='the source directory for images')
    parser.add_argument('--dest', '-d', default='./processed', type=str,
                        help='the destination directory - the directory structure from source will be replicated to here with processed images inside')
    return parser.parse_args()


def main():
    args = get_parsed_args()
    source_path = args.source
    dest_path = args.dest
    recreate_directory_structure(source_path, dest_path)
    process_all(source_path, dest_path)


if __name__ == '__main__':
    start_time = time.perf_counter()
    main()
    end_time = time.perf_counter()
    print("Main took {:0.2f} minutes".format((end_time - start_time) / 60))
