import unittest
from main import recreate_directory_structure
import os
import shutil

LOCAL_SOURCE_PATH = 'test/data/files'
LOCAL_PROCESSED_PATH = 'test/data/processed'

class TestMain(unittest.TestCase):

    def tearDown(self):
        if os.path.isdir(LOCAL_PROCESSED_PATH):
            shutil.rmtree(LOCAL_PROCESSED_PATH)

    def test_recreate_directory_structure(self):
        recreate_directory_structure(LOCAL_SOURCE_PATH, LOCAL_PROCESSED_PATH)
        for _, dirs, _ in os.walk(LOCAL_SOURCE_PATH):
            for dir in dirs:
                expected_path = os.path.join(LOCAL_PROCESSED_PATH, dir)
                self.assertTrue(os.path.isdir(expected_path), "Path {} not replicated".format(expected_path))


if __name__ == '__main__':
    unittest.main()
