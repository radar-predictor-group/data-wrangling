import os
import shutil
import unittest
from image_processing import process_all, count_files
from main import recreate_directory_structure

LOCAL_SOURCE_PATH = 'test/data/image_root/original'
NUM_FILES_IN_SOURCE_PATH = 6
LOCAL_PROCESSED_PATH = 'test/data/image_root/processed'


class ImageProcessingTest(unittest.TestCase):

    def setUp(self):
        # remove any previous results
        if os.path.isdir(LOCAL_PROCESSED_PATH):
            shutil.rmtree(LOCAL_PROCESSED_PATH)
        recreate_directory_structure(LOCAL_SOURCE_PATH, LOCAL_PROCESSED_PATH)

    def __all_files_processed(self, source_path, dest_path):
        files_to_check = count_files(source_path)
        files_checked = 0
        for root, _, files in os.walk(source_path):
            files_checked += len(files)
            check_path = root.replace(source_path, dest_path)
            for file in files:
                expected_path = os.path.join(check_path, file)
                self.assertTrue(os.path.isfile(expected_path),
                                "File {} not found in processing output".format(expected_path))
        self.assertEqual(files_to_check, files_checked)

    def test_count_files(self):
        self.assertEqual(NUM_FILES_IN_SOURCE_PATH, count_files(LOCAL_SOURCE_PATH))

    def test_process_all(self):
        process_all(LOCAL_SOURCE_PATH, LOCAL_PROCESSED_PATH)
        self.__all_files_processed(LOCAL_SOURCE_PATH, LOCAL_PROCESSED_PATH)


if __name__ == '__main__':
    unittest.main()
