import os
import time
from multiprocessing import Pool

from PIL import Image

COLOUR_MAP = [
    (0x00, 0x00, 0x00),  # 0
    (0xf5, 0xf5, 0xff),  # 1
    (0xb4, 0xb4, 0xff),  # 2
    (0x78, 0x78, 0xff),  # 3
    (0x14, 0x14, 0xff),  # 4
    (0x00, 0xd8, 0xc3),  # 5
    (0x00, 0x96, 0x90),  # 6
    (0x00, 0x66, 0x66),  # 7
    (0xff, 0xff, 0x00),  # 8
    (0xff, 0xc8, 0x00),  # 9
    (0xff, 0x96, 0x00),  # 10
    (0xff, 0x64, 0x00),  # 11
    (0xff, 0x00, 0x00),  # 12
    (0xc8, 0x00, 0x00),  # 13
    (0x78, 0x00, 0x00),  # 14
    (0x28, 0x00, 0x00),  # 15
]


def map_colours(image, mapping):
    # the image types are indexed, so these pixel values are indices
    src_pixel_data = image.convert('RGB').load()
    with Image.new('L', image.size) as mapped_image:
        dest_pixel_data = mapped_image.load()
        for idx, colour_map in enumerate(mapping):
            colour = int(idx * 255 / 15)
            for y in range(image.size[1]):
                for x in range(image.size[0]):
                    if src_pixel_data[x, y] == (colour_map[0], colour_map[1], colour_map[2]):
                        dest_pixel_data[x, y] = colour
        return mapped_image
    return None


def process_image(file_tuple):
    source_file, dest_file = file_tuple
    with Image.open(source_file) as source_image:
        width, height = source_image.size
        PIXELS_TO_TRIM = 16
        cropped_image = source_image.crop((PIXELS_TO_TRIM,
                                           PIXELS_TO_TRIM,
                                           width - PIXELS_TO_TRIM,
                                           height - PIXELS_TO_TRIM))
        mapped_image = map_colours(cropped_image, COLOUR_MAP)
        resized_image = mapped_image.resize((32, 32), Image.ANTIALIAS)
        resized_image.save(dest_file)


def count_files(path):
    file_count = 0
    for _, _, files in os.walk(path):
        file_count += len(files)
    return file_count


def process_all(source_path, dest_path):
    def add_os_sep(path):
        return path + os.path.sep if not path.endswith(os.path.sep) else path

    source_path = add_os_sep(source_path)
    dest_path = add_os_sep(dest_path)
    file_count = count_files(source_path)
    processed_count = 0
    print('Beginning image processing for {} files'.format(file_count))
    start_time = time.perf_counter()

    pool = Pool(processes=4)  # start 4 worker processes

    work_list = []
    for root, _, files in os.walk(source_path):
        dest_base_path = root.replace(source_path, dest_path)
        for file in files:
            source_filepath = os.path.join(root, file)
            if source_filepath.endswith('.png'):
                dest_file = os.path.join(dest_base_path, file)
                work_list.append((source_filepath, dest_file))
            processed_count += 1
            if processed_count % 20 == 0:
                pool.map(process_image, work_list)
                work_list = []
                progress_time = time.perf_counter()
                time_delta = progress_time - start_time
                # we have processed processed_count items so far, and
                # need to process file_count - processed_count more, so
                # the remaining time is:
                time_per_item = time_delta / processed_count
                remaining_time = time_per_item * (file_count - processed_count)
                print('{:0.2f}% complete - estimated remaining time: {:0.2f} minutes'.format(100 * processed_count / file_count,
                                                                   remaining_time / 60))
    if len(work_list) > 0:
        pool.map(process_image, work_list)
